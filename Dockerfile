FROM python:3.7

ENV PYTHONUNBUFFERED 1

ARG APP=/app
WORKDIR $APP
VOLUME $APP

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt ./
RUN pip3 install -r requirements.txt


COPY . /app


EXPOSE 5000
ENTRYPOINT [ "./manage.py", "runserver", "0:5000" ]



